import { STATE_LOGIN, STATE_SIGNUP } from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from 'pages/AuthPage';
import React from 'react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';
const io = require("socket.io-client");


let socket = io.connect("http://dtp-rtc-server:9875");


const TablePage = React.lazy(() => import('pages/TablePage'));
const TypographyPage = React.lazy(() => import('pages/TypographyPage'));


const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  
  state = {
    data: null
  };

  async  componentDidMount() {
    
    const res = await this.callBackendAPI()

    console.log(res);
      // .then(res => this.setState({ data: res.express }))
      // .catch(err => console.log(err));
  }
    // fetching the GET route from the Express server which matches the GET route from server.js
  async callBackendAPI() {
    const response = await fetch('http://localhost:9875/express_backend', {
      mode: "cors",
      method: 'GET',
    });
    const body = await response.json();
    console.log('the response from the back: ', body)

    if (response.status !== 200) {
      console.log('response satus; ', response.status)
      throw Error(body.message) 
    }
    return body;
  };

  render() {
    
    return (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          <Switch>
            <LayoutRoute
              exact
              path="/login"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_LOGIN} />
              )}
            />
            <LayoutRoute
              exact
              path="/signup"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_SIGNUP} />
              )}
            />

            <MainLayout breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/" component={TablePage} />
                
                <Route exact path="/tables" component={TablePage} />
                
                <Route exact path="/typography"  component={TypographyPage} />
                
                
                
                
               
              </React.Suspense>
            </MainLayout>
            <Redirect to="/" />
          </Switch>
        </GAListener>
      </BrowserRouter>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
