import React from 'react'

function ConnectingCircle({status}) {

    let color =  status===0 ? "#26ff6f" : (status===1 ? "#ff432e" : "#ffd000"); 

    return(
        <div className="circleContainer" 
        style={{width: "20px", height: '20px', backgroundColor: color, borderRadius: '50%'}}>

        </div>
    )

}


export default ConnectingCircle