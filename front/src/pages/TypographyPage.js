import Page from 'components/Page';
import Typography from 'components/Typography';
import React from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import ConnectingCircle from './ConnectingCircle';


 class  TypographyPage extends React.Component {
//const TypographyPage = () => {
  constructor(props) {
    super(props);
    this.state = {
        items: [],
        isLoaded: false
    }
   }
   componentDidMount() {
   fetch('http://217.160.242.120:8000/devices/api/device-management/devices/')
        .then(res => res.json())
        .then(json => {
          console.log(json)
            this.setState({
                items: json.results,
                isLoaded: true, 
            })})
        .catch((err) => {
            console.log(err);
        });
   }
   render(){

    const { isLoaded, items } = this.state;
     if (!isLoaded)
    return <div>Loading...</div>;
  return (
    <Page
      
      breadcrumbs={[{ name: 'MY RSPIS', active: true }]}>
      <Row>
        <Col>
          <Card>
            
            <CardBody>
             <ul>
                    {items.length && items.map(item => (
                        <li key={item.id}>
                            
                          <p>  ID: {item.id} | Name: {item.label} | unique_code: {item.unique_code}   </p> 
                         
                        </li>
                    ))}
              </ul>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row className="mb-3" />

      
    </Page>
  );
 }
};

export default TypographyPage;
