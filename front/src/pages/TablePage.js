import Page from 'components/Page';
import React from 'react';
import { Card, Col, Row, Table } from 'reactstrap';
import ConnectingCircle from './ConnectingCircle';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
const tableTypes = [''];
 function BasicMenu({name}) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        aria-controls="basic-menu"
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        {name}
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleClose}>start </MenuItem>
        <MenuItem onClick={handleClose}>stop </MenuItem>
        <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
    </div>
  );
}
class TablePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false
    }
  }
  componentDidMount() {
    fetch('http://217.160.242.120:8000/devices/api/device-management/devices/')
      .then(res => res.json())
      .then(json => {
        console.log(json)
        this.setState({
          items: json.results,
          isLoaded: true,
        })
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    const { isLoaded, items } = this.state;
    if (!isLoaded)
      return <div>Loading...</div>;
    return (
      <Page
        title=""
        breadcrumbs={[{ name: 'My connected RSpis', active: true }]}
        className="TablePage"
      >
        {tableTypes.map((tableType, index) => (
          <Row key={index}>
            <Col>
              <Card className="mb-3">


                <Row>
                  <Col>
                    <Card body>
                      <Table {...{ [tableType]: true }}>
                        <thead>
                          <tr>
                            <td>Id</td>
                            
                            <td>label</td>
                            <td>gps_dongle</td>
                            <td>status</td>
                            
                          </tr>
                        </thead>
                        <tbody>
                          {items.length && items.map(item => (
                            <tr key={item.id}>
                              <td scope="row">{item.id}</td>
                              <td ><BasicMenu name={item.label} /> </td>
                              <td>{item.gps_dongle}</td>
                              
                              <td>
                                <ConnectingCircle status={0} />
                              </td>
                              {/* ID: {item.id} | Name: {item.label} */}
                            </tr>
                          ))}
                          {/* <tr>
                            <th scope="row">1</th>
                            <td>dtb-rtc-jqghfkhqlf</td>
                            <td>connected</td>
                            <td>@mdo</td>
                          </tr>
                          <tr>
                            <th scope="row">2</th>
                            <td>dtb-rtc-hkqbjvnkqlbi</td>
                            <td>disconnected</td>
                            <td>@fat</td>
                          </tr>
                          <tr>
                            <th scope="row">3</th>
                            <td>dtb-rtc-klqbgkqfgbkw</td>
                            <td>re-connection</td>
                            <td>@twitter</td>
                          </tr> */}
                        </tbody>
                      </Table>
                    </Card>
                  </Col>


                </Row>

              </Card>
            </Col>
          </Row>
        ))}




      </Page>
    );
  }
};

export default TablePage;
