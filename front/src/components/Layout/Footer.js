import React from 'react';

import { Navbar, Nav, NavItem } from 'reactstrap';



const Footer = () => {
  return (
    <Navbar>
      <Nav navbar>
        <NavItem>
          2021@ITCFicial Team
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default Footer;
