"use strict";

var _require = require("kafkajs"),
    Kafka = _require.Kafka,
    logLevel = _require.logLevel; //const redis = require('redis');
//const client = redis.createClient({host: 'redis',port: 6379});
// the client ID lets kafka know who's producing the messages


var clientId = "my-app"; // we can define the list of brokers in the cluster

var brokers = ["kafka:9092"]; // this is the topic to which we want to write messages
// initialize a new kafka client and initialize a producer from it			

var kafka = new Kafka({
  clientId: clientId,
  brokers: brokers
  /*logLevel: logLevel.DEBUG */

});
var producer = kafka.producer({}); // we define an async function that writes a new message each second

var produceF = function produceF(topic, i) {
  return regeneratorRuntime.async(function produceF$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(producer.connect());

        case 2:
          // after the produce has connected, we start an interval timer
          setInterval(function _callee() {
            return regeneratorRuntime.async(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.prev = 0;
                    _context.next = 3;
                    return regeneratorRuntime.awrap(producer.send({
                      topic: topic,
                      acks: 1,
                      messages: [{
                        key: String(i),
                        value: "this is message " + i
                      }]
                    }));

                  case 3:
                    // if the message is written successfully, log it and increment `i`
                    console.log("salam islem", i, "to topic ", topic);
                    i++;
                    _context.next = 10;
                    break;

                  case 7:
                    _context.prev = 7;
                    _context.t0 = _context["catch"](0);
                    console.error("could not write message " + _context.t0);

                  case 10:
                  case "end":
                    return _context.stop();
                }
              }
            }, null, null, [[0, 7]]);
          }, 10000);

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  });
};

module.exports = produceF;