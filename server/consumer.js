const { Kafka, logLevel } = require("kafkajs")
const io= require('./socket.js')

const clientId = "my-app"
const brokers = ["kafka:9092"]

const kafka = new Kafka({
	clientId,
	brokers,
	// logCreator: customLogger,
	//logLevel: logLevel.DEBUG,
})
// the kafka instance and configuration variables are the same as before
// create a new consumer from the kafka client, and set its group ID
// the group ID helps Kafka keep track of the messages that this client
// is yet to receive
const consumer = kafka.consumer({
	groupId: clientId,
	minBytes: 5,
	maxBytes: 1e6,
	// wait for at most 9 seconds before receiving new data
	maxWaitTimeInMs: 9000,
})

const consume = async (socket,/*topic,*/Array) => {
	console.log('this is the list ya islem  '+Array)


	try{

	// first, we wait for the client to connect and subscribe to the given topic
	await consumer.connect()
	
	await consumer.subscribe({ topic:/dtb-rtc-.*/gi, fromBeginning: true })
	
	await consumer.run({
		// this function is called every time the consumer gets a new message
		eachMessage:async ({ topic, partition,message }) => {
			
			// here, we just log the message to the standard output
		
			console.log(`waalaikom salam: ${message.value}`)
			//console.log(`waalaikom salam: ${message.headers}`)
			 socket.emit('pshh',"going to front  " +message.value+'\n')
			  console.log(socket.id)
			  console.log(Array)
		},
          


	}) }catch (err){
		console.error("could not read message " + err)
	}
}

module.exports = consume
