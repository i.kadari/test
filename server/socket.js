
const http=require('http');
const socketIo=require('socket.io');
const path=require('path');
const express=require('express');
const cors = require('cors');

console.log('example log')

const publicPath=path.join(__dirname, "./public/");

let app=express();
app.use(cors())
app.use(express.static(publicPath))


app.get('/express_backend', (req, res) => { 
    console.log('got a get request from :', req.url);
    res.setHeader('Content-Type', "application/json")
    res.send(({ express: 'MY BACKEND IS CONNECTED TO REACT' }));
});
let server=http.createServer(app);
let io=socketIo(server);

module.exports= {io, server, app};