const { Kafka, logLevel } = require("kafkajs")
//const redis = require('redis');
//const client = redis.createClient({host: 'redis',port: 6379});
// the client ID lets kafka know who's producing the messages
const clientId = "my-app"
// we can define the list of brokers in the cluster
const brokers = ["kafka:9092"]
// this is the topic to which we want to write messages


// initialize a new kafka client and initialize a producer from it			

const kafka = new Kafka({ clientId, brokers, /*logLevel: logLevel.DEBUG */})
const producer = kafka.producer({})

// we define an async function that writes a new message each second
const produceF = async (topic,i) => {
	
	await producer.connect()
    
	
    

	// after the produce has connected, we start an interval timer
	//setInterval(async () => {
        
		try {
			// send a message to the configured topic with
			// the key and value formed from the current value of `i`
			await producer.send({
				topic,
                
				acks: 1,
				messages: [
					{
						key: String(i),
						
						value: "this is message " + i,
					},
				],
			})

			// if the message is written successfully, log it and increment `i`
			console.log("salam islem", i,"to topic ",topic)
			i++
		} catch (err) {
			console.error("could not write message " + err)
		}
	//},10000)
}


module.exports = produceF